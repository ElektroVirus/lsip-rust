
use chrono::NaiveDateTime;
use std::fmt;

#[derive(Debug)]
pub struct Session {
    id:                         String,
    user:                        u32,
    name:                       String,
    timestamp:                  NaiveDateTime,
    seat:                       String,
    tty:                        String,
    vtnr:                       u8,
    remote:                     bool,
    remote_host:                String,
    service:                    String,
    scope:                      String,
    leader:                     u32,
    audit:                      u32,
    session_type:               String,
    session_class:              String,
    active:                     bool,
    state:                      String,
    idle_hint:                  bool,
    idle_since_hint:            u128,
    idle_since_hint_monotonic:  u128,
    locked_hint:                bool,
    display:                    String,
    desktop:                    String,
}


impl Session {

    fn default() -> Session {
        let session: Session = Session {
            id:                         String::new(),
            user:                        0,
            name:                       String::new(),
            timestamp:                  NaiveDateTime::from_timestamp(0, 0),
            seat:                       String::new(),
            tty:                        String::new(),
            vtnr:                       0,
            remote:                     false,
            remote_host:                String::new(),
            service:                    String::new(),
            scope:                      String::new(),
            leader:                     0,
            audit:                      0,
            session_type:               String::new(),
            session_class:              String::new(),
            active:                     false,
            state:                      String::new(),
            idle_hint:                  false,
            idle_since_hint:            0,
            idle_since_hint_monotonic:  0,
            locked_hint:                false,
            display:                    String::new(),
            desktop:                    String::new(),
        };
        session
    }

    pub fn new (s: String) -> Session {
        /* Parse output of `loginctl show-session <id>` */ 
        let split = s.trim().split("\n");
        let split_vec: Vec<&str> = split.collect();

        let mut session: Session = Session::default();

        for s in split_vec {

            let args: Vec<&str> = s.split("=").collect();

            fn parse_bool(arg: &str) -> bool {
                match arg.trim() {
                    "yes"   => true,
                    "no"    => false,
                    _       => panic!(),
                }
            }

            match args[0] {
                "Id"                        => session.id   = String::from(args[1]),
                "User"                      => session.user = args[1].parse().unwrap(),
                "Name"                      => session.name = String::from(args[1]),
                "TimestampMonotonic"        => session.timestamp = 
                    NaiveDateTime::from_timestamp(args[1].parse::<i64>().unwrap(), 0),
                "Seat"                      => session.seat = String::from(args[1]),
                "TTY"                       => session.tty = String::from(args[1]),
                "VTNr"                      => session.vtnr = args[1].parse().unwrap(),
                "Remote"                    => session.remote = parse_bool(args[1]),
                "RemoteHost"                => session.remote_host = String::from(args[1]),
                "Service"                   => session.service = String::from(args[1]),
                "Scope"                     => session.scope = String::from(args[1]),
                "Leader"                    => session.leader = args[1].parse().unwrap(),
                "Audit"                     => session.audit = args[1].parse().unwrap(),
                "Type"                      => session.session_type = String::from(args[1]),
                "Class"                     => session.session_class = String::from(args[1]),
                "Active"                    => session.active = parse_bool(args[1]),
                "State"                     => session.state = String::from(args[1]),
                "IdleHint"                  => session.idle_hint = parse_bool(args[1]),
                "IdleSinceHint"             => session.idle_since_hint = args[1].parse().unwrap(),
                "IdleSinceHintMonotonic"    => session.idle_since_hint_monotonic = 
                    args[1].parse().unwrap(),
                "LockedHint"                => session.locked_hint = parse_bool(args[1]),
                "Display"                   => session.display  = String::from(args[1]),
                "Desktop"                   => session.desktop  = String::from(args[1]),
                _                           => println!("Unknown token [{}, val={}]", 
                                                    args[0], args[1]),
            };
        }       
        return session;
    }

    pub fn is_sftp(&self) -> bool {
        &self.tty == "" && &self.seat == "" && &self.service == "sshd"
    }

    pub fn is_ssh(&self) -> bool {
        &self.tty != "" && &self.seat == "" && &self.service == "sshd"
    }

    pub fn is_seat(&self) -> bool {
        &self.seat != ""
    }

    

    pub fn get_id(&self) -> &str {
        &self.id
    }

    pub fn get_remote_host(&self) -> &str {
        &self.remote_host
    }
}

impl fmt::Display for Session {

    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut session_type: String = String::from("Unknown");
        if self.is_sftp() {
            session_type = String::from("SFTP");
        }
        else if self.is_ssh() {
            session_type = String::from("SSH");
        }
        else if self.is_seat() {
            session_type = String::from("Seat");
        }

        write!(f, 
                concat!("{} session:\n",
                "Id:                        {}\n", 
                "User:                      {}\n", 
                "Name:                      {}\n", 
                "Timestamp:                 {}\n", 
                "TimestampMonotonic:        {}\n", 
                "VTNr:                      {}\n", 
                "Seat:                      {}\n", 
                "TTY:                       {}\n", 
                "Remote:                    {}\n", 
                "RemoteHost:                {}\n", 
                "Service:                   {}\n", 
                "Scope:                     {}\n", 
                "Leader:                    {}\n", 
                "Audit:                     {}\n", 
                "Type:                      {}\n", 
                "Class:                     {}\n", 
                "Active:                    {}\n", 
                "State:                     {}\n", 
                "IdleHint:                  {}\n", 
                "IdleSinceHint:             {}\n", 
                "IdleSinceHintMonotonic:    {}\n",
                "LockedHint:                {}\n", 
                "Display:                   {}\n", 
                "Desktop:                   {}\n"),
                session_type,
                self.id,
                self.user,
                self.name,
                self.timestamp,
                self.timestamp.timestamp(),
                self.vtnr,
                self.seat,
                self.tty,
                self.remote,
                self.remote_host,
                self.service,
                self.scope,
                self.leader,
                self.audit,
                self.session_type,
                self.session_class,
                self.active,
                self.state,
                self.idle_hint,
                self.idle_since_hint,
                self.idle_since_hint_monotonic,
                self.locked_hint,
                self.display,
                self.desktop)
    }
}
