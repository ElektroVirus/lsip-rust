use std::process::Command;

use std::fs::File;
use std::io::Read;

mod session;

fn main() {
    // let s = read_example_output();
    let s = loginctl_list_sessions();

    let split = s.trim().split("\n");

    let mut line_vec: Vec<&str> = split.collect();
    
    line_vec.remove(0);                 // First line is 'header' with constant text
    for _i in 0..2 {                    // As are two last lines
        line_vec.remove(line_vec.len() -1);
    }

    let mut session_vec: Vec<session::Session> = Vec::new();

    for s in line_vec {
        let split = s.trim().split_whitespace();
        let split_vec: Vec<&str> = split.collect();

        let show_session_string = loginctl_show_session(split_vec[0]);
        let ses = session::Session::new(show_session_string);
        session_vec.push(ses);
    }

    for session in session_vec {
        if session.is_ssh() {
            println!("SSH session id={} IP={}", session.get_id(), session.get_remote_host());
        }
        else if session.is_sftp() {
            println!("SFTP session id={} IP={}", session.get_id(), session.get_remote_host());
        }
        else if session.is_seat() {
            println!("Local session id={}", session.get_id());
        }
    }
}

fn loginctl_list_sessions() -> String {
    let output = Command::new("loginctl")
        .arg("list-sessions")
        .output()
        .expect("loginctl command failed to start");

    assert!(output.status.success());
    String::from_utf8_lossy(&output.stdout).to_string()
}

fn loginctl_show_session(id: &str) -> String {
    let output = Command::new("loginctl")
        .arg("show-session")
        .arg(format!("{}", id))
        .output()
        .expect("loginctl command failed to start");

    if !output.status.success() {
        println!("Command failed! id={}", id);
    }
    assert!(output.status.success());
    String::from_utf8_lossy(&output.stdout).to_string()
}


#[allow(dead_code)]
fn read_example_output() -> String {
    let filename = "example_output";

    // Open the file in read-only mode.
    match File::open(filename) {
        // The file is open (no error).
        Ok(mut file) => {
            let mut content = String::new();

            // Read all the file content into a variable (ignoring the result of the operation).
            file.read_to_string(&mut content).unwrap();

            content
            // The file is automatically closed when is goes out of scope.
        },
        // Error handling
        Err(error) => {
            println!("Error opening file {}: {}", filename, error);
            String::from("")
        }
    }
}
